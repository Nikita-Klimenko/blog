<?php

class Model_Portfolio extends Model
{
	
	public function get_data()
	{	
		
		
		
		return array(
			
			array(
				'Year' => '2016',
				'Site' => 'https://guru.thinkmobiles.com/course/bazovij/',
				'Description' => 'Completed a basic course on the basis of the language C++'
			),

			array(
				'Year' => '2017',
				'Site' => 'https://guru.thinkmobiles.com/course/php/',
				'Description' => 'Completed a basic course on the basis of the language PHP'
			)

		);
	}

}
