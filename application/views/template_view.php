<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--
Design by Free CSS Templates
http://www.freecsstemplates.org
Released for free under a Creative Commons Attribution 3.0 License

Name       : Accumen
Description: A two-column, fixed-width design with dark color scheme.
Version    : 1.0
Released   : 20120712

Modified 
-->
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<title>BLOG</title>
		<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css" />
		<link href="http://fonts.googleapis.com/css?family=Kreon" rel="stylesheet" type="text/css" />
		<link rel="stylesheet" type="text/css" href="/css/style.css" />
		<script src="/js/jquery-1.6.2.js" type="text/javascript"></script>
		<script type="text/javascript">
		// return a random integer between 0 and number
		function random(number) {
			
			return Math.floor( Math.random()*(number+1) );
		};
		
		// show random quote
		$(document).ready(function() { 

			var quotes = $('.quote');
			quotes.hide();
			
			var qlen = quotes.length; //document.write( random(qlen-1) );
			$( '.quote:eq(' + random(qlen-1) + ')' ).show(); //tag:eq(1)
		});
		</script>
	</head>
	<body>
		<div id="wrapper">
			<div id="header">
				<div id="logo">
					<a href="/">MY</span> <span class="cms">BLOG</span></a>
				</div>
				<div id="menu">
					<ul>
						<li class="first active"><a href="/">HOME</a></li>
						<li><a href="/services">ARTICLES</a></li>
						<li><a href="/portfolio">PORTFOLIO</a></li>
						<li class="last"><a href="/contacts">CONTACTS</a></li>
					</ul>
					<br class="clearfix" />
				</div>
			</div>
			<div id="page">
				<div id="sidebar">
					<div class="side-box">
						<h3>RANDOM QUOTE</h3>
						<p align="justify" class="quote">
						«The site, as a living organism, changes and develops.
                        You can not immediately write the perfect version and on this one bow out is a utopia»
						</p>
						<p align="justify" class="quote">
						«Everything should be very simple, like a text file and at the same time functionally
                        And then users will leave us»
						</p>
						<p align="justify" class="quote">
						«It's crazy to be the one who tried to understand this crazy world»
						</p>
					</div>
					<div class="side-box">
						<h3>LIST MENU</h3>
						<ul class="list">
							<li class="first "><a href="/">HOME</a></li>
							<li><a href="/services">ARTICLES</a></li>
							<li><a href="/portfolio">PORTFOLIO</a></li>
							<li class="last"><a href="/contacts">CONTACTS</a></li>
						</ul>
					</div>
				</div>
				<div id="content">
					<div class="box">
						<?php include 'application/views/'.$content_view; ?>
						
					</div>
					<br class="clearfix" />
				</div>
				<br class="clearfix" />
			</div>
			<div id="page-bottom">
				<div id="page-bottom-sidebar">
					<h3>CONTACTS</h3>
					<ul class="list">
						<li class="first">skypeid: nikita26081985</li>
						<li class="last">email: kjonynn@gmail.com</li>
					</ul>
				</div>
				<div id="page-bottom-content">
					<h3>ABOUT ME</h3>
					<p>
Responsible, easily trained, 
I have organizational skills. 
Lead a healthy lifestyle.
Beginner programmer.
					</p>
				</div>
				<br class="clearfix" />
			</div>
		</div>
		<div id="footer">
			<a href="/">MY BLOG</a> &copy; 2017</a>
		</div>
	</body>
</html>